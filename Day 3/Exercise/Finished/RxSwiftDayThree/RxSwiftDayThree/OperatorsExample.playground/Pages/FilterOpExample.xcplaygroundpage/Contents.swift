//: Playground - noun: a place where people can play

import UIKit
import RxSwift
import PlaygroundSupport

example(of: "distinctUntilChanged") {
    let disposeBag = DisposeBag()
    // 1
    Observable.of("A", "A", "B", "B", "A")
        // 2
        .distinctUntilChanged()
        .subscribe(onNext: {
            print($0) })
        .disposed(by: disposeBag)
}

example(of: "distinctUntilChanged(_:)") {
    let disposeBag = DisposeBag()
    // 1
    let formatter = NumberFormatter()
    formatter.numberStyle = .spellOut
    // 2
    Observable<NSNumber>.of(10, 110, 20, 200, 210, 310)
        // 3
        .distinctUntilChanged { a, b in
            // 4
            guard let aWords = formatter.string(from:a)?.components(separatedBy: " "),
                let bWords = formatter.string(from: b)?.components(separatedBy: " ")
                    else {
                    return false
            }
            var containsMatch = false
            // 5
            for aWord in aWords {
                for bWord in bWords {
                    if aWord == bWord {
                        containsMatch = true
                        break
                    } }
            }
            return containsMatch
        }
        // 4
        .subscribe(onNext: {
            print($0)
        })
        .disposed(by: disposeBag)
}

example(of: "elementAt") {
    // 1
    let strikes = PublishSubject<String>()
    let disposeBag = DisposeBag()
    // 2
    strikes
        .elementAt(2)
        .subscribe(onNext: { element  in
            print("You're out!")
            print(element)
        })
        .disposed(by: disposeBag)
    
    strikes.onNext("X")
    strikes.onNext("Y")
    strikes.onNext("Z")
    strikes.onNext("A")

}
//Covered in Cricket Example
example(of: "filter") {
    let disposeBag = DisposeBag()
    // 1
    Observable.of(1, 2, 3, 4, 5, 6)
        // 2
        .filter { integer in
            integer % 2 == 0
        }
        // 3
        .subscribe(onNext: {
            print($0)
        })
        .disposed(by: disposeBag)
}

example(of: "ignoreElements") {
    // 1
    let strikes = PublishSubject<String>()
    let disposeBag = DisposeBag()
        // 2
    strikes.ignoreElements()
        .subscribe { _ in
            print("You're out!")
        }
        .disposed(by: disposeBag)
    
    strikes.onNext("Y")
    strikes.onNext("Z")
    strikes.onNext("A")
}

example(of: "sample") {
    // 1
    //PlaygroundPage.current.needsIndefiniteExecution = true
    
    let strikes = PublishSubject<String>()
    let disposeBag = DisposeBag()
    // 2
    
    let ob = Observable<Int>.interval(14.0, scheduler: MainScheduler.instance)
    strikes.sample(ob)
        .subscribe { event  in
            print(event.element)
        }
        .disposed(by: disposeBag)
    
    strikes.onNext("Y")
    strikes.onNext("Z")
    strikes.onNext("A")
    strikes.onNext("S")
}

example(of: "skip") {
    // 1
    let strikes = PublishSubject<String>()
    let disposeBag = DisposeBag()
    // 2
    
    strikes.skip(2)
        .subscribe { event  in
            print(event.element)
        }
        .disposed(by: disposeBag)
    
    strikes.onNext("Y")
    strikes.onNext("Z")
    strikes.onNext("A")
    strikes.onNext("S")
}

example(of: "skipWhile") {
    let disposeBag = DisposeBag()
    // 1
    Observable.of(2,2,3,4,2,4)
        // 2
        .skipWhile { integer in
            integer % 2 == 0
        }
        .subscribe(onNext: {
            print($0)
        }).disposed(by: disposeBag)
}
example(of: "skipUntil") {
    let disposeBag = DisposeBag()
    // 1
    let subject = PublishSubject<String>()
    let trigger = PublishSubject<String>()
    // 2
    subject
        .skipUntil(trigger)
        .subscribe(onNext: {
            print($0) })
        .disposed(by: disposeBag)
    
    subject.onNext("A")
    subject.onNext("B")
    trigger.onNext("X")
    subject.onNext("C")
}

example(of: "take") {
    let disposeBag = DisposeBag()
    // 1
    Observable.of(1, 2, 3, 4, 5, 6)
        // 2
        .take(3)
        .subscribe(onNext: {
            print($0) })
        .disposed(by: disposeBag)
}

example(of: "takeWhile") {
    let disposeBag = DisposeBag()
    // 1
    Observable.of(2,2,3,4,2,4)
        // 3
        .takeWhile { integer in
            // 4
            integer % 2 == 0
        }
        // 6
        .subscribe(onNext: {
            print($0) })
        .disposed(by: disposeBag)
}

example(of: "takeUntil") {
    let disposeBag = DisposeBag()
    // 1
    let subject = PublishSubject<String>()
    let trigger = PublishSubject<String>()
    // 2
    subject.takeUntil(trigger)
        .subscribe(onNext: {
            print($0) })
        .disposed(by: disposeBag)
    // 3
    subject.onNext("1")
    subject.onNext("2")
    trigger.onNext("X")
    subject.onNext("3")
}
