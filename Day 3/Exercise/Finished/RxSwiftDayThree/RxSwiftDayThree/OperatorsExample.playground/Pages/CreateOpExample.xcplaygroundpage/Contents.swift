//: [Previous](@previous)

import Foundation
import RxSwift
import PlaygroundSupport

example(of: "deferred") {
    let disposeBag = DisposeBag()
    // 1
    var flip = false
    // 2
    let factory: Observable<Int> = Observable.deferred {
        // 3
        flip = !flip
        // 4
        if flip {
            return Observable.of(1, 2, 3)
        } else {
            return Observable.of(4, 5, 6)
        }
    }
    
    for _ in 0...3 {
        factory.subscribe(onNext: {
            print($0, terminator: "")
        }).disposed(by: disposeBag)
        print()
    }
    
}

example(of: "Interval"){
//    let disposeBag = DisposeBag()
//
//    let timer = Observable<Int>.interval(0.1, scheduler: MainScheduler.instance)
//    timer.debug().subscribe{ event in
//        print(event.element)
//    }.disposed(by: disposeBag)
    
    //PlaygroundPage.current.needsIndefiniteExecution = true
}

example(of: "Repeat"){
//    let disposeBag = DisposeBag()
//
//    let repeatExample = Observable<Int>.repeatElement(12)
//    repeatExample.subscribe { event in
//        print(event.element!)
//    }.disposed(by: disposeBag)
}

example(of: "Timer"){
    let disposeBag = DisposeBag()
    
    let obs = Observable<Int>.timer(10.0, scheduler: MainScheduler.instance)
    obs.subscribe{ event in
        print(event.element)
        }.disposed(by: disposeBag)
}



PlaygroundPage.current.needsIndefiniteExecution = true

/*example(of: "Start"){
 let disposeBag = DisposeBag()
 
 func stringFromTimeInterval(sr: String) {
 print("iam printing\(sr)")
 }
 
 var timer = Observable.of("1")
 timer.map(stringFromTimeInterval).subscribe{ event in
 print(event)
 }.disposed(by: disposeBag)
 
 }*/
