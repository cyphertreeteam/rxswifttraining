//: Playground - noun: a place where people can play

import UIKit
import RxSwift

example(of: "doOnNext") {
    let disposeBag = DisposeBag()
    
    let temp = Observable.from([89, 23, 12, -2, -7])
    temp.asObservable().do(onNext: {
        print("I am performing side effect : \($0 * $0)")
    }).do(onNext: {
        print("\($0)", terminator:" * ")
    }).map({
        print("10  = \($0 * 10)")
    }).subscribe{
        print($0)
    }.disposed(by: disposeBag)
}

example(of: "doOnNext With Subjects") {
    let disposeBag = DisposeBag()
    
    let temp = PublishSubject<Int>()
    
    temp.asObserver().do(onNext: {
            print("I am performing side effect : \($0 * $0)")
        }).do(onNext: {
            print("\($0)", terminator:" * ")
        }).map({
            print("10  = \($0 * 10)")
        }).subscribe{
            print($0)
    }.disposed(by: disposeBag)
    
    temp.onNext(23)
}
