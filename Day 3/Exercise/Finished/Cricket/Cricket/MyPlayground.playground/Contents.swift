//: Playground - noun: a place where people can play

import UIKit
import RxSwift

var str = "Hello, playground"

//Mark : Protocol
protocol Playable {
    var name: String { get }
    var runs: Int { get set}
}
protocol Keepable:Playable {
}

protocol Bowlable: Playable {
}

class Cricket{
    //Mark : Struct
    struct Batsman: Playable{
        var name: String = ""
        var runs: Int = 0
    }
    
    struct Bowler: Playable, Bowlable{
        var name: String = ""
        var runs: Int = 0
    }
    
    struct Keeper: Playable, Keepable{
        var name: String = ""
        var runs: Int = 0
    }
    
    struct Over {
        var count: Int = 0
        var score: Int = 0
        var ballCount: Int = 0
        var wideCount: Int = 0
        var noBallCount: Int = 0
    }
    
    var batsmanOne: Batsman!
    var batsmanTwo: Batsman!
    var bowler: Bowler!
    var over: Over!
    var score: Int = 0
    var numberOfBoundaries: Int = 0
    
    init() {
        batsmanOne = Batsman(name: "Dhawan", runs: 0)
        batsmanTwo = Batsman(name: "Kolhi", runs: 0)
        bowler = Bowler(name: "Wood", runs: 0)
        over = Over.init(count: 0, score: 0, ballCount: 0, wideCount: 0, noBallCount: 0)
    }
}
fileprivate let disposeBag = DisposeBag()
var cricket: Cricket = Cricket()

var score:Observable<Int>!
var boundaries:Observable<Observable<Int>>!

var batsManOneRuns: BehaviorSubject<Int> = BehaviorSubject<Int>(value: cricket.batsmanOne.runs)
var batsManTwoRuns: BehaviorSubject<Int> = BehaviorSubject<Int>(value: cricket.batsmanTwo.runs)
var switchLatestBatsMan: Observable<Int>!
var skipCheck:BehaviorSubject<Int> = BehaviorSubject<Int>(value: 0)
var over:Observable<Cricket.Over>
let ball = Observable<Void>.empty()

ball.subscribe(
    onCompleted: {
        cricket.over.ballCount = +1
        //print("Current ball count \(cricket.over.ballCount)")
}).disposed(by: disposeBag)


batsManOneRuns.asObservable().subscribe(onNext: { element in
    cricket.batsmanOne.runs = element
}).disposed(by: disposeBag)


score = Observable.from([batsManOneRuns.asObservable(), batsManTwoRuns.asObservable()]).merge()

boundaries = Observable.from(optional: score)
    
boundaries.subscribe(onNext: { observer in
    observer.asObservable().filter{ score in
        score % 4 == 0
        }.subscribe(onNext: {
        cricket.numberOfBoundaries = +1
        //print("Number of Boundaries : \(cricket.numberOfBoundaries)")
        print("Show Boundaries \($0)")
    }).disposed(by: disposeBag)
}).disposed(by: disposeBag)


switchLatestBatsMan = Observable.from([batsManOneRuns,batsManTwoRuns]).switchLatest()

score.asObservable().scan(0, accumulator: + ).subscribe(onNext: { element in
    cricket.score = element
    //print("Curerent score \(element)")
}).disposed(by: disposeBag)

batsManOneRuns.onNext(2)

batsManTwoRuns.asObservable().subscribe(onNext: { element in
    //print("Tell me how much batsman two scored last: \(element)")
}).disposed(by: disposeBag)


switchLatestBatsMan.subscribe(onNext: { element in
    print("switchLatestBatsMan \(element)")
}).disposed(by: disposeBag)


batsManTwoRuns.onNext(4)
batsManTwoRuns.onNext(2)

batsManOneRuns.asObservable().skipUntil(batsManTwoRuns).subscribe(onNext: { (element) in
    //print("what is this \(element)")
}).disposed(by: disposeBag)

batsManTwoRuns.onNext(6)
batsManOneRuns.onNext(3)
batsManTwoRuns.onNext(9)

over = Observable<Cricket.Over>.create{ observer in
    observer.onNext(Cricket.Over(count: cricket.over.count + 1, score: cricket.score, ballCount: 0, wideCount: 0, noBallCount: 0))
    //observer.onCompleted()
    return Disposables.create()
}
over.subscribe(onNext: { over in
    cricket.over = over
    //print("Current over is \(over.count)")
}).disposed(by: disposeBag)

batsManOneRuns.onNext(4)

