//: Playground - noun: a place where people can play

import UIKit
import RxSwift

var str = "Hello, playground"

//Mark : Protocol
protocol Playable {
    var name: String { get }
    var runs: Int { get set}
}
protocol Keepable:Playable {
}

protocol Bowlable: Playable {
}

class Cricket{
    //Mark : Struct
    struct Batsman: Playable{
        var name: String = ""
        var runs: Int = 0
    }
    
    struct Bowler: Playable, Bowlable{
        var name: String = ""
        var runs: Int = 0
    }
    
    struct Keeper: Playable, Keepable{
        var name: String = ""
        var runs: Int = 0
    }
    
    struct Over {
        var count: Int = 0
        var score: Int = 0
        var ballCount: Int = 0
        var wideCount: Int = 0
        var noBallCount: Int = 0
    }
    
    var batsmanOne: Batsman!
    var batsmanTwo: Batsman!
    var bowler: Bowler!
    var over: Over!
    var score: Int = 0
    var numberOfBoundaries: Int = 0
    
    init() {
        batsmanOne = Batsman(name: "Dhawan", runs: 0)
        batsmanTwo = Batsman(name: "Kolhi", runs: 0)
        bowler = Bowler(name: "Wood", runs: 0)
        over = Over.init(count: 0, score: 0, ballCount: 0, wideCount: 0, noBallCount: 0)
    }
}
