
import Foundation
import RxSwift

example(of: "Variable") {
    
    // 1
    let variable = Variable("Initial value")
    
    let disposeBag = DisposeBag()
    
    // 2
    variable.value = "New initial value"
    
    // 3
    variable.asObservable()
        .subscribe {
            print(label: "1)", event: $0)
        }
        .disposed(by: disposeBag)
    
    // 1
    variable.value = "1"
    
    // 2
    variable.asObservable()
        .subscribe {
            print(label: "2)", event: $0)
        }
        .disposed(by: disposeBag)
    
    // 3
    variable.value = "2"
    
    /*
     variable.value.onError(MyError.anError)
     variable.asObservable().onError(MyError.anError)
     variable.value = MyError.anError
     variable.value.onCompleted()
     variable.asObservable().onCompleted()
    */
}

enum MyError: Error {
    case anError
}


// 2
func print<T: CustomStringConvertible>(label: String, event: Event<T>) {
    print(label, event.element ?? event.error ?? event)
}
