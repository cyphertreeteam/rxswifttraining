//: [Previous](@previous)

import Foundation
import RxSwift
import PlaygroundSupport

example(of: "Maybe") {
    // 1
    let disposeBag = DisposeBag()
    
    enum DataReadWriteError: Error {
        case unwritable, unreadable
    }
    
    func saveData(textToWrite: String) -> Maybe<String> {
        
        return Maybe.create { maybe in
            let disposable = Disposables.create()
            
            let fileManager = FileManager.default

            let path = playgroundSharedDataDirectory.appendingPathComponent("Save.txt")
           
            do {
                let contents = try String(contentsOf: path)
                maybe(.success(contents))
                return disposable
            } catch  {
                do {
                    try textToWrite.write(to: path, atomically: true, encoding: .utf8)
                    maybe(.completed)
                    return disposable
                } catch  {
                    maybe(.error(DataReadWriteError.unwritable))
                    return disposable
                }
            }
            return disposable
        }
    }
    
    saveData(textToWrite: "RxSwift Training").subscribe{
        switch $0 {
        case .success(let string):
            print(string)
        case .error(let error):
            print(error)
        case .completed:
            print("completed")
        }
    }.disposed(by: disposeBag)
}
