//: Playground - noun: a place where people can play

import UIKit

//Functional Programming.

//Pure function
//immutable, yields the same output provided given same input
//var number:Int = 1

func add(x: Int, y: Int) -> Int{
    //number = number * x
    //print(x*y)
    return x + y
}

let sum = add(x: 4, y: 5)
print(sum)

let array = [1, 2, 3]
let arrayDouble = array.map{$0 + 2}
print(array)
print(arrayDouble)
