//: Playground - noun: a place where people can play

import UIKit

//Imperative programming example

var array = [1, 2, 3]
for number in array {
    print(number)
    array = [4, 5, 6]
}
print(array)

// Example of declarative programming.
//Problem Statement : Print each number from 1 to 10

(1...10).forEach { print($0) }

//data -> fetch { condition } -> sort { criteria }

let doubleNumber:((Int) -> Int) = ({ value in
    return value * 2
})

doubleNumber(8)
