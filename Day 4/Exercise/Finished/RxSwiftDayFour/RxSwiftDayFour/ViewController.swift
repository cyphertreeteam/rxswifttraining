//
//  ViewController.swift
//  RxSwiftDayFour
//
//  Created by Madhavi  Solanki on 23/06/18.
//  Copyright © 2018 Madhavi  Solanki. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ViewController: UIViewController {

    @IBOutlet weak var textCheck: UITextField!
    @IBOutlet weak var textResult: UITextField!
    @IBOutlet weak var labelCheck: UILabel!
    @IBOutlet weak var button: UIButton!
    private  let disposeBag = DisposeBag()

    override func viewDidLoad() {
        //exampleOfDriver()
        //exampleOfSignal()
        //exampleOfBehaviourAndDriver()
        exampleOfControlEvent()
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func exampleOfDriver(){
        //1
        let results = textCheck.rx.text.asDriver(onErrorJustReturn: "Test")
        
        //2
        results
            .map { $0 }.debug()
            .drive(textResult.rx.text)
            .disposed(by: disposeBag)
        
        //3
        self.button.rx.tap
            .scan(0) { (priorValue, _) in
                print(priorValue)
                return priorValue + 1
            }
            .asDriver(onErrorJustReturn: 0)
            .map { currentCount in
                print(currentCount)
                return "You have tapped that button \(currentCount) times."
            }
            .drive(self.labelCheck.rx.text)
            .disposed(by: disposeBag)

    }
    
    func exampleOfControlEvent(){
        let beh: PublishRelay<Any> = PublishRelay<Any>()
        let checkMe = textCheck.rx.controlEvent(UIControlEvents.editingDidEnd).asSignal(onErrorJustReturn: ())
        checkMe.map { $0 }.emit(to: beh).disposed(by: disposeBag)
        
        beh.subscribe(onNext: { event in
            print(event)
        }).disposed(by: disposeBag)
    }

    func exampleOfSignal(){
        let source = PublishSubject<Int>()
        let d = source.asDriver(onErrorRecover: { _ in .empty() })
        let s = source.asSignal(onErrorRecover: { _ in .empty() })
        
        source.onNext(1)
        
        d.debug("First Drive").drive().disposed(by: disposeBag)
        s.debug("First Emit ").emit().disposed(by: disposeBag)
        
        source.onNext(2)
        
        d.debug("Second Drive").drive().disposed(by: disposeBag)
        s.debug("Second Emit ").emit().disposed(by: disposeBag)
    }
    
    func exampleOfBehaviourAndDriver(){
        let behaviorRelay: BehaviorRelay<String?> = BehaviorRelay<String?>(value: "Add some Text Here")
        let driver = textCheck.rx.text.asDriver(onErrorJustReturn: "Add driver Text Here")

        driver
            .debug()
            .drive(behaviorRelay)
            .disposed(by: disposeBag)

        behaviorRelay.subscribe({ event in
            print("I am relay\(event)")
        }).disposed(by: disposeBag)
        
    }
}
