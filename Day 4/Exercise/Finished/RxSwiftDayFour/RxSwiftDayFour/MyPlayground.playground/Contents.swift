//: Playground - noun: a place where people can play

import UIKit
import RxSwift
import RxCocoa
import PlaygroundSupport

example(of: "BehaviorRelay"){
    let disposeBag = DisposeBag()
    let relayOne = BehaviorRelay<Int>(value: 1)
    
    relayOne.accept(4)
    relayOne.subscribe{
        print($0)
        }.disposed(by: disposeBag)
    
    relayOne.accept(3)
    
}

example(of: "PublisRelay"){
    let disposeBag = DisposeBag()
    let relayTwo = PublishRelay<Int>()
    
    relayTwo.accept(4)
    relayTwo.subscribe{
        print($0)
        }.disposed(by: disposeBag)
    
    relayTwo.accept(3)
}


example(of: "Share / Debug / ShareReplay") {
    
    struct ItemProvider {
        var itemsIn: BehaviorSubject<[Int]> = BehaviorSubject<[Int]>(value:[1,2,4,5])
    }
    
    let itemProvier: Observable<ItemProvider> = Observable<ItemProvider>.of(ItemProvider())
    
//    let items: Observable<[Int]> = itemProvier.flatMap{ item  in
//        item.itemsIn
//        }.debug()
    
    let items: Observable<[Int]> = itemProvier.flatMap{ item  in
        item.itemsIn
        }.share(replay: 1, scope: .whileConnected)
    
//    let items: Observable<[Int]> = itemProvier.flatMap{ item  in
//        item.itemsIn
//        }.share()
    
    let count: Observable<Int> = items
        .map { $0.count }
    
    items.subscribe(onNext: { items in
        print(items)
    })
    
    count.subscribe(onNext: { count in
        print(count)
    })
}


example(of: "ConnectableObservable") {
    struct ItemProvider {
        var itemsIn: BehaviorSubject<[Int]> = BehaviorSubject<[Int]>(value:[1,2,4,5])
    }
    
    let itemProvier: Observable<ItemProvider> = Observable<ItemProvider>.of(ItemProvider())
    
    let items: ConnectableObservable<[Int]> = itemProvier.flatMap{ item  in
        item.itemsIn
        }.publish()
    
    items.subscribe(onNext: { items in
        print(items)
    })
    
    items.connect()
}
example(of: "Publish On Subscription") {
    print("Starting at 0 seconds")
    
    //PlaygroundPage.current.needsIndefiniteExecution = true
    let myObservable = Observable<Int>.interval(1, scheduler: MainScheduler.instance).publish()
    
    myObservable.connect()
    
    let mySubscription = myObservable.subscribe(onNext: {
        print("Next: \($0)")
    })
    
    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
        print("Disposing at 3 seconds")
        mySubscription.dispose()
    }
    
    DispatchQueue.main.asyncAfter(deadline: .now() + 6.0) {
        print("Subscribing again at 6 seconds")
        myObservable.subscribe(onNext: {
            print("Next: \($0)")
        })
    }
}



