//
//  ViewController.swift
//  RxSwiftDayFour
//
//  Created by Madhavi  Solanki on 23/06/18.
//  Copyright © 2018 Madhavi  Solanki. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ViewController: UIViewController {

    @IBOutlet weak var textCheck: UITextField!
    @IBOutlet weak var textResult: UITextField!
    @IBOutlet weak var labelCheck: UILabel!
    @IBOutlet weak var button: UIButton!
    private  let disposeBag = DisposeBag()

    override func viewDidLoad() {
       
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
